from django.urls import path
from .views import todo_list, todo_list_detail, todo_list_create, todo_list_update, todo_list_delete, todo_item_create, todo_item_update

app_name = 'todos'

urlpatterns = [
    path('', todo_list, name='todo_list_list'),
    path('todo_list_detail/<int:id>/', todo_list_detail, name='todo_list_detail'),
    path('todo_list_create/', todo_list_create, name='todo_list_create'),
    path('todo_list_update/<int:id>/edit/', todo_list_update, name='todo_list_update'),
    path('todo_list_delete/<int:id>/delete/', todo_list_delete, name='todo_list_delete'),
    path('todo_item_create/items/create', todo_item_create, name='todo_item_create'),
    path('todo_item_update/items/<int:id>/edit/', todo_item_update, name='todo_item_update'),
]
