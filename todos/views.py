from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList
from .forms import TodoListForm
from .forms import TodoItemForm
from .models import TodoItem

# Create your views here.
def todo_list(request):
    todo_lists = TodoList.objects.all()
    return render(request, 'todos/todo_list_list.html', {'todo_lists': todo_lists})

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    return render(request, 'todos/todo_list_detail.html', {'todo_list': todo_list})

def todo_list_create(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_todo = form.save()
            return redirect('todos/todo_list_detail', id=new_todo.id)
    else:
        form = TodoListForm()

    context = {
        "form": form
    }

    return render(request, 'todos/todo_list_create.html', context)

def todo_list_update(request, id):
    update_list = get_object_or_404(TodoList, id=id)

    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=update_list)
        if form.is_valid():
            update_list = form.save()
            return redirect('todos/todo_list_update', id=update_list.id)
    else:
        form = TodoListForm(instance=update_list)

    context = {
        "form": form
    }

    return render(request, 'todos/todo_list_update.html', context)

def todo_list_delete(request, id):
    delete_list = TodoList.objects.get(id=id)
    if request.method == 'POST':
        delete_list.delete()
        return redirect('todos/todo_list_delete')

    return render(request, 'todos/todo_list_delete.html')

def todo_item_create(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            create_todo = form.save
            return redirect('todo_list_detail', id=create_todo.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form
    }

    return render(request, 'todos/todo_item_create.html', context)

def todo_item_update(request):
    update_item = get_object_or_404(TodoItem, id=id)

    if request.method =='POST':
        form = TodoItemForm(request.POST, instance=update_item)
        if form.is_valid():
            form.save()
            return redirect('todos/todo_list_detail', id=update_item.id)
    else:
        form = TodoItemForm(instance=update_item)
    context = {
        "form": form
    }

    return render(request, 'todos/todo_item_update.html', context)
